﻿namespace Management.Utility
{
    using log4net;
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Net;
    using System.Text.RegularExpressions;

    public static class Utility
    {
        public static void RegistryErrorLog(string message)
        {
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = LogManager.GetLogger("LogPV");
            logger.Error(message);
        }

        public static void RegistryFatalLog(string message)
        {
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = LogManager.GetLogger("LogPV");
            logger.Fatal(message);
        }

        public static void RegistryInfoLog(string message)
        {
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = LogManager.GetLogger("LogPV");
            logger.Info(message);
        }

       
        public static string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, @"[^\w\s.!|?@$%^&*()\-\/]+", string.Empty, RegexOptions.Compiled);
        }

        public static string ConvertImageToBase64(string path)
        {
            using (Image image = Image.FromFile(path))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();

                    // Convert byte[] to Base64 String
                    string base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
        }

        public static string ConvertImageToBase64(Stream stream)
        {
            byte[] imageArray = ReadFully(stream);
            return Convert.ToBase64String(imageArray);
        }

        public static bool DownloadImageFromUrl(string filename, ImageFormat format, string imageUrl)
        {
            try
            {
                WebClient client = new WebClient();
                Stream stream = client.OpenRead(imageUrl);
                Bitmap bitmap = new Bitmap(stream);

                if (bitmap != null)
                {
                    bitmap.Save(filename, format);
                }

                stream.Flush();
                stream.Close();
                client.Dispose();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static Stream DownloadImageFromUrl(ImageFormat format, string imageUrl)
        {
            try
            {
                WebClient client = new WebClient();
                Stream stream = client.OpenRead(imageUrl);
                return stream;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static bool DeleteFile(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                RegistryErrorLog(ex.Message);
                return false;
            }
        }

        private static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        public static Color GetColor()
        {
            Random randomGen = new Random();
            KnownColor[] names = (KnownColor[])Enum.GetValues(typeof(KnownColor));
            KnownColor randomColorName = names[randomGen.Next(1,names.Length)];
            Color randomColor = Color.FromKnownColor(randomColorName);
            return randomColor;
        }

        public static Color GetColor(int index)
        {
            KnownColor[] names = (KnownColor[])Enum.GetValues(typeof(KnownColor));
            KnownColor colorName = names[index];
            Color color = Color.FromKnownColor(colorName);
            return color;
        }

    }
}
