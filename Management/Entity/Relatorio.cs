﻿namespace Management.Entity
{
    public class Relatorio
    {
        public string Periodo { get; set; }
        public decimal ReceitaLiquida { get; set; }
        public decimal CustoFixo { get; set; }
        public decimal Comissao { get; set; }
        public decimal Lucro { get
            {
                return ReceitaLiquida - CustoFixo - Comissao;
            }
        }
    }
}
