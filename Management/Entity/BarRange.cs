﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management.Entity
{
    public class BarRange
    {
        public string Title { get; set; }
        public int Mes { get; set; }
        public int Anio { get; set; }
    }
}
