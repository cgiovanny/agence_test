﻿namespace Management.Entity
{
    using System;

    public class ResponseType<T>
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public T Value { get; set; }
    }
}
