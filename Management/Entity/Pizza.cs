﻿namespace Management.Entity
{
    using System.Collections.Generic;

    public class Pizza
    {
        public List<decimal> Values { get; set; }
        public List<string> Colors { get; set; }
        public string Title { get; set; }
        public List<string> Labels { get; set; }
    }

}
