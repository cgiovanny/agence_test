﻿namespace Management.Entity
{
    using System.Collections.Generic;

    public class Bar
    {
       public string Title { get; set; }
        public List<BarValue> Values { get; set; }
        public List<string> Labels { get; set; }
    }
}
