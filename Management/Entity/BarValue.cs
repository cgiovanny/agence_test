﻿namespace Management.Entity
{
    using System.Collections.Generic;

    public class BarValue
    {
        public string label { get; set; }
        public string type { get; set; }
        public string borderColor { get; set; }
        public List<decimal> data { get; set; }
        public bool fill { get; set; }
        public string backgroundColor { get; set; }
        public string backgroundColorHover { get; set; }
    }
}
