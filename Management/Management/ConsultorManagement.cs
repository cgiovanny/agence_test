﻿namespace Management.Management
{
    using DataAccess;
    using Entity;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Utility;

    public static class ConsultorManagement
    {
        public static ResponseType<List<Consultor>> GetConsultores(DBConnection connection = null)
        {
            ResponseType<List<Consultor>> _response = new ResponseType<List<Consultor>>();
            DBAccess _dbAccess = new DBAccess();
            try
            {
                _dbAccess.Schema = GetSchemaConsultor();
                _response.Value = _dbAccess.ExecReader(new Consultor(), "caol.USP_Get_usuario_permissao", _dbAccess.Parameters, _dbAccess.Schema, connection).Value;
                _response.Success = true;
                _response.Message = "OK";
            }
            catch (Exception ex)
            {
                Utility.RegistryErrorLog(ex.Message);
                _response.Success = false;
                _response.Message = ex.Message;
                _response.Value = new List<Consultor>();
            }

            return _response;
        }

        public static ResponseType<List<Relatorio>> GetRelatorios(int mesDesde, int anioDesde, int mesHasta, int anioHasta, string consultor, DBConnection connection = null)
        {
            ResponseType<List<Relatorio>> _response = new ResponseType<List<Relatorio>>();
            DBAccess _dbAccess = new DBAccess();
            try
            {
                _dbAccess.Parameters.Add("@MesDesde",mesDesde);
                _dbAccess.Parameters.Add("@AnioDesde", anioDesde);
                _dbAccess.Parameters.Add("@MesHasta", mesHasta);
                _dbAccess.Parameters.Add("@AnioHasta", anioHasta);
                _dbAccess.Parameters.Add("@Consultor", consultor);
                _dbAccess.Schema = GetSchemaRelatorio();
                _response.Value = _dbAccess.ExecReader(new Relatorio(), "caol.USP_Get_Relatorio", _dbAccess.Parameters, _dbAccess.Schema, connection).Value;
                _response.Success = true;
                _response.Message = "OK";
            }
            catch (Exception ex)
            {
                Utility.RegistryErrorLog(ex.Message);
                _response.Success = false;
                _response.Message = ex.Message;
                _response.Value = new List<Relatorio>();
            }

            return _response;
        }

        public static ResponseType<decimal> GetReceitaLiquidaPorMes(int mes, int anio, string consultor, DBConnection connection = null)
        {
            ResponseType<decimal> _response = new ResponseType<decimal>();
            DBAccess _dbAccess = new DBAccess();
            try
            {
                _dbAccess.Parameters.Add("@Mes", mes);
                _dbAccess.Parameters.Add("@Anio", anio);
                _dbAccess.Parameters.Add("@Consultor", consultor);
                _response.Value = _dbAccess.ExecScalarReader("caol.USP_Get_ReceitaLiquidaPorMes", _dbAccess.Parameters, connection).ReturnValue;
                _response.Success = true;
                _response.Message = "OK";
            }
            catch (Exception ex)
            {
                Utility.RegistryErrorLog(ex.Message);
                _response.Success = false;
                _response.Message = ex.Message;
                _response.Value = 0;
            }

            return _response;
        }

        public static ResponseType<decimal> GetCustoFixo(string consultor, DBConnection connection = null)
        {
            ResponseType<decimal> _response = new ResponseType<decimal>();
            DBAccess _dbAccess = new DBAccess();
            try
            {
                _dbAccess.Parameters.Add("@Consultor", consultor);
                _response.Value = _dbAccess.ExecScalarReader("caol.USP_Get_CustoFixo", _dbAccess.Parameters, connection).ReturnValue;
                _response.Success = true;
                _response.Message = "OK";
            }
            catch (Exception ex)
            {
                Utility.RegistryErrorLog(ex.Message);
                _response.Success = false;
                _response.Message = ex.Message;
                _response.Value = 0;
            }

            return _response;
        }

        public static ResponseType<decimal> GetRecitaLiquida(int mesDesde, int anioDesde, int mesHasta, int anioHasta, string consultor, DBConnection connection = null)
        {
            ResponseType<decimal> _response = new ResponseType<decimal>();
            DBAccess _dbAccess = new DBAccess();
            try
            {
                _dbAccess.Parameters.Add("@MesDesde", mesDesde);
                _dbAccess.Parameters.Add("@AnioDesde", anioDesde);
                _dbAccess.Parameters.Add("@MesHasta", mesHasta);
                _dbAccess.Parameters.Add("@AnioHasta", anioHasta);
                _dbAccess.Parameters.Add("@Consultor", consultor);
                _response.Value = _dbAccess.ExecScalarReader("caol.USP_Get_ReceitaLiquida", _dbAccess.Parameters, connection).ReturnValue;
                _response.Success = true;
                _response.Message = "OK";
            }
            catch (Exception ex)
            {
                Utility.RegistryErrorLog(ex.Message);
                _response.Success = false;
                _response.Message = ex.Message;
                _response.Value = 0;
            }

            return _response;
        }

        private static Dictionary<string, string> GetSchemaConsultor()
        {
            Dictionary<string, string> _schema = new Dictionary<string, string>();
            _schema.Add("Usuario", "co_usuario");
            _schema.Add("Nombre", "no_usuario");
            return _schema;
        }

        private static Dictionary<string, string> GetSchemaRelatorio()
        {
            Dictionary<string, string> _schema = new Dictionary<string, string>();
            _schema.Add("Periodo", "Periodo");
            _schema.Add("ReceitaLiquida", "ReceitaLiquida");
            _schema.Add("CustoFixo", "CustoFixo");
            _schema.Add("Comissao", "Comissao");
            return _schema;
        }

        private static Dictionary<string, string> GetSchemaBar()
        {
            Dictionary<string, string> _schema = new Dictionary<string, string>();
            _schema.Add("Periodo", "Periodo");
            _schema.Add("ReceitaLiquida", "ReceitaLiquida");
            _schema.Add("CustoFixo", "CustoFixo");
            _schema.Add("Comissao", "Comissao");
            return _schema;
        }
    }
}
