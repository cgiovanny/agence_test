﻿namespace WEB
{
    using System.Web.Optimization;

    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {   
            //js JQuery
            bundles.Add(new ScriptBundle("~/assets/js/jquery").Include(
                        "~/assets/js/jquery-2.1.4.min.js"));
            //js JQuery-UI
            bundles.Add(new ScriptBundle("~/assets/js/jquery-ui").Include(
                        "~/assets/js/jquery-ui.custom.min.js",
                        "~/assets/js/jquery.ui.touch-punch.min.js"));
             //js bootstrap
             bundles.Add(new ScriptBundle("~/assets/js/bootstrap").Include(
                        "~/assets/js/bootstrap.min.js"));
            //js ace
            bundles.Add(new ScriptBundle("~/assets/js/ace").Include(
                        "~/assets/js/ace-elements.min.js",
                        "~/assets/js/ace.min.js"));
            //js ace extra
            bundles.Add(new ScriptBundle("~/assets/js/aceextra").Include(
                        "~/assets/js/ace-extra.min.js"));
            //js jquery.easypiechart
            bundles.Add(new ScriptBundle("~/assets/js/jquery.easypiechart").Include(
                        "~/assets/js/jquery.easypiechart.min.js"));
            //js jquery.sparkline
            bundles.Add(new ScriptBundle("~/assets/js/jquery.sparkline").Include(
                        "~/assets/js/jquery.sparkline.index.min.js"));
            //js jquery.flot
            bundles.Add(new ScriptBundle("~/assets/js/jquery.flot").Include(
                        "~/assets/js/jquery.flot.min.js",
                        "~/assets/js/jquery.flot.pie.min.js",
                        "~/assets/js/jquery.flot.resize.min.js"));
            //js jquery.datatables
            bundles.Add(new ScriptBundle("~/assets/js/jquery.datatables").Include(
                        "~/assets/js/jquery.dataTables.min.js",
                        "~/assets/js/jquery.dataTables.bootstrap.min.js",
                        "~/assets/js/dataTables.buttons.min.js",
                        "~/assets/js/dataTables.select.min.js"));
            //js catalog
            bundles.Add(new ScriptBundle("~/assets/js").Include(
                        "~/assets/js/catalog.js"));
            bundles.Add(new ScriptBundle("~/assets/js/catalog").Include(
                "~/assets/js/bootbox.js",
                "~/assets/js/catalog.js"));
            //js Gritter
            bundles.Add(new ScriptBundle("~/assets/js/gritter").Include(
                        "~/assets/js/jquery.gritter.min.js"));
            //js Gritter
            bundles.Add(new ScriptBundle("~/assets/js/tree").Include(
                        "~/assets/js/tree.min.js"));

            bundles.Add(new ScriptBundle("~/assets/js/toastr").Include(
                      "~/assets/js/toastr.min.js"));

            bundles.Add(new ScriptBundle("~/assets/js/site").Include(
                     "~/assets/js/site.js"));

            // Sweet alert
            bundles.Add(new ScriptBundle("~/assets/js/sweetAlert").Include(
                      "~/assets/js/sweetalert.min.js"));

            // Sweet alert
            bundles.Add(new ScriptBundle("~/assets/js/validate").Include(
                      "~/assets/js/jquery.validate.min.js"));

            // SimTree alert
            bundles.Add(new ScriptBundle("~/assets/js/simTree").Include(
                      "~/assets/js/simTree.js"));

            // Editable
            bundles.Add(new ScriptBundle("~/assets/js/editable").Include(
                      "~/assets//bootstrap-editable.min.js",
                      "~/assets/js/ace-editable.min.js"));
            // iCheck
            bundles.Add(new ScriptBundle("~/assets/js/dualList").Include(
                      "~/assets/js/jquery.bootstrap-duallistbox.min.js"));

            // iCheck
            bundles.Add(new ScriptBundle("~/assets/js/iCheck").Include(
                      "~/assets/js/iCheck/icheck.min.js"));


            // Sweet alert Styless
            bundles.Add(new StyleBundle("~/assets/css/sweetAlert").Include(
                      "~/assets/css/sweetalert.css"));

            // toastr notification styles
            bundles.Add(new StyleBundle("~/assets/css/toastr").Include(
                      "~/assets/css/toastr.min.css"));

            bundles.Add(new StyleBundle("~/assets/css/bootstrap").Include(
                      "~/assets/css/bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/assets/css/font-awesome").Include(
                      "~/assets/font-awesome/4.5.0/css/font-awesome.min.css"));

            bundles.Add(new StyleBundle("~/assets/css/fonts.googleapis").Include(
                      "~/assets/css/fonts.googleapis.com.css"));

            bundles.Add(new StyleBundle("~/assets/css/ace").Include(
                      "~/assets/css/ace.min.css",
                      "~/assets/css/ace-skins.min.css",
                      "~/assets/css/ace-rtl.min.css",
                      "~/assets/css/ace-extra.min.css"));

            bundles.Add(new StyleBundle("~/assets/css/gritter").Include(
                      "~/assets/css/jquery.gritter.min.css  "));

            bundles.Add(new StyleBundle("~/assets/css/simTree").Include(
                      "~/assets/css/simTree.css  "));
            // iCheck css styles
            bundles.Add(new StyleBundle("~/assets/css/iCheck/iCheckStyles").Include(
                      "~/assets/css/iCheck/custom.css"));

            // iCheck css styles
            bundles.Add(new StyleBundle("~/assets/css/dualListbox").Include(
                      "~/assets/css/bootstrap-duallistbox.min.css"));
        }
    }
}
