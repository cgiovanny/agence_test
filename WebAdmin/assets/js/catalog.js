﻿var myTable = null;
var catalogconfig = null;
var columssearch = 0;
var searchactive = 0;
$("#btncatalog").on(ace.click_event, function () {
    if (myTable != null) {
        myTable.clear();
        myTable.destroy();
    }
    $('.filterrow').remove();    
    var action = $(this).attr("data-action");
    var controller = $(this).attr("data-controller");
    var _result = false
    var myUrl = "/" + controller + "/" + action;
    var columns = 0;
    $.ajax({
        url: myUrl,
        type: "GET",
        dataType: "json",
        contenType: false,
        async: false,
        success: function (result) {
            _result = true
            catalogconfig = result;
            $("#catalogtitle").html(result.Name);
            $("#selectfield").empty();
            $("#dynamic-table thead").empty();
            var header = '<tr>';
            columns = result.Columns.length;
            for (var i = 0; i < result.Columns.length; i++) {
                header += '<th data-binding="' + result.Columns[i].DataBinding + '">' + result.Columns[i].Name + '</th>';
                if (result.Columns[i].IsFilter) {
                    columssearch++;
                    $("#selectfield").append('<option id="' + result.Columns[i].DataBinding + '">' + result.Columns[i].Name + '</option>');
                }
                    
            }
            header += '</tr>';
            $("#dynamic-table thead").append(header);
            $('#btnSearchCatalogData').attr('data-action', result.ActionSearch);
            $('#btnSearchCatalogData').attr('data-controller', result.Controller);
            searchactive = 1;
        }
    });

    if (_result)
        $('#myModalCatalog').modal({ show: true })
    else
        alert("Error en catalogo");
    resetDataTable(columns);
});
$("body").on("click",'.removefilter', function () {
    $(this).parent().parent().parent().remove();
    $('#btnAddFiltercatalog').removeClass('disabled');
    searchactive--;
});
$('#btnAddFiltercatalog').on(ace.click_event, function () {
    if (searchactive == columssearch) {
        $('#btnAddFiltercatalog').addClass('disabled');
        return;
    }
    searchactive++;
    var options = '';
    for (var i = 0; i < catalogconfig.Columns.length; i++) {
        if (catalogconfig.Columns[i].IsFilter)
            options +='<option id="' + catalogconfig.Columns[i].DataBinding + '">' + catalogconfig.Columns[i].Name + '</option>';
    }
    var newfilter = '<div class="row filterrow">' +
        '<div class="col-sm-7">' + 
        '<div class="form-group">' +
        '<input type="text" placeholder="Ingrese el valor a buscar..." name="searchtext" class="form-control form-block">' +
        '</div>' +
        '</div>' +
        '<div class="col-sm-3">' +
        '<div class="form-group">' +
        '<select name="selectfield" class="form-control form-block">' + 
        options +
        '</select > ' +
        '</div>' +
        '</div>' +
        '<div class="col-sm-2">' +
        '<div class="form-group">' +
        '<button class="btn btn-danger btn-sm removefilter" data-action="" data-controller="" >' +
        ' <i class="ace-icon fa fa-minus bigger-110"></i>' +
        '</button>' +
        '</div>' +
        '</div>' +
        '</div>';

    $('#filtercontent').prepend(newfilter);
    if (searchactive == columssearch) {
        $('#btnAddFiltercatalog').addClass('disabled');
    }
});
$("#btnSearchCatalogData").on(ace.click_event, function () {
       
    var action = $('#btnSearchCatalogData').attr('data-action');
    var controller = $('#btnSearchCatalogData').attr('data-controller');
    var _result = false
    var myUrl = "/" + controller + "/" + action;
    $.ajax({
        url: myUrl,
        type: "GET",
        dataType: "json",
        data: {value:"Giovanny",column:"Login"},
        contenType: false,
        async: false,
        success: function (result) {
            _result = true
            myTable.clear();
            for (var i = 0; i < result.length; i++) {
                var databind = [];
                var obj = new Object();
                obj = result[i];
                $("th").each(function () {
                    databind.push(obj[$(this).attr('data-binding')])
                });
                myTable.row.add(databind).draw();
            }
            
        }
    });
});

function resetDataTable(columns) {
    myTable =
        $('#dynamic-table')
            //.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
            .DataTable({
                bAutoWidth: false,
                

                //"bProcessing": true,
                //"bServerSide": true,
                //"sAjaxSource": "http://127.0.0.1/table.php"	,

                //,
                //"sScrollY": "200px",
                //"bPaginate": false,

                //"sScrollX": "100%",
                //"sScrollXInner": "120%",
                //"bScrollCollapse": true,
                //Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
                //you may want to wrap the table inside a "div.dataTables_borderWrap" element

                //"iDisplayLength": 50

                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados.",
                    "sEmptyTable": "No se encontraron resultados.",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                "searching": true,
                "paging": true,
                "info": true,
                "lengthChange": true
            });
    $('.dataTables_empty').attr('colspan', columns);

}
