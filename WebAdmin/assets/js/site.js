﻿toastr.options = {
    "closeButton": true,
    "debug": false,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "onclick": null,
    "showDuration": "1",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}
function ShowNotificationInfo(strMessage) {
    toastr.success(strMessage);
}

function ShowNotificationWarning(strMessage) {
    toastr.warning(strMessage);
}

function ShowNotificationError(strMessage) {
    toastr.error(strMessage);
}

function ShowNotificationModalInfo(strMessage) {
    swal({
        title: "Información",
        text: strMessage,
        type: "success"
    });
}

function ShowNotificationModalInfoWithOutButtons(strMessage) {
    swal({
        title: "Información",
        text: strMessage,
        showCancelButton: false,
        showConfirmButton: false
    });
}

function ShowNotificationModalWaitingLoad(strMessage) {
    let timerInterval
    Swal.fire({
        title: 'Información',
        html: '<h5>' + strMessage + '</h5>', 
        onBeforeOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
            }, 1)
        }
    })
}

function ShowNotificationModalWarning(strMessage, confirmButtonText, cancelButtonText, callback, id) {
    swal({
        title: "Información",
        text: strMessage,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: confirmButtonText,
        cancelButtonText: cancelButtonText,
        closeOnConfirm: true
    }, function () {
        callback(id);
    });
}


function ShowNotificationModalWarningWithOutButtons(strMessage) {
    swal({
        title: "Información",
        text: strMessage,
        type: "warning",
    });
}

function ShowNotificationModalError(strMessage) {
    swal({
        title: "Información",
        text: strMessage,
        type: "error",
    });
}

