﻿namespace WEB.Controllers
{
    using DataAccess;
    using Management.Entity;
    using Management.Management;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetRelatorio(int mesDesde, int anioDesde, int mesHasta, int anioHasta, string consultor)
        {
            RelatorioConsultor _relatorioConsultor = new RelatorioConsultor();
            new DBAccess().NonTransaction((connect) =>
            {
                _relatorioConsultor.Cosnultor = ConsultorManagement.GetConsultores(connect).Value.Where(x => x.Usuario == consultor).FirstOrDefault();
                _relatorioConsultor.Relatorios = ConsultorManagement.GetRelatorios(mesDesde, anioDesde, mesHasta, anioHasta, consultor, connect).Value;
            });
            return PartialView("_Relatorio", _relatorioConsultor);
        }

        public ActionResult GetPizza(int mesDesde, int anioDesde, int mesHasta, int anioHasta, string consultor)
        {
            Pizza _pizza = GetPizzaValues(mesDesde, anioDesde, mesHasta, anioHasta, consultor);
            return PartialView("_Pizza", _pizza);
        }

        public ActionResult GetGrafico(int mesDesde, int anioDesde, int mesHasta, int anioHasta, string consultor)
        {
            Bar _bar = GetBarValues(mesDesde, anioDesde, mesHasta, anioHasta, consultor);
            return PartialView("_Grafico", _bar);
        }

        [HttpGet]
        public ActionResult GetConsultores()
        {
            List<Consultor> _consultores = ConsultorManagement.GetConsultores().Value;
             return Json(_consultores, JsonRequestBehavior.AllowGet);
        }

        #region "Private Methodes"
        private List<BarRange> GetRanges(int mesDesde, int anioDesde, int mesHasta, int anioHasta)
        {
            DateTimeFormatInfo formatoFecha = CultureInfo.CurrentCulture.DateTimeFormat;
            List<BarRange> _ranges = new List<BarRange>();
            
            if (anioDesde == anioHasta)
            {
                for (int mesActual = mesDesde; mesActual <= mesHasta; mesActual++)
                {
                    _ranges.Add(new BarRange()
                    {
                        Anio = anioDesde,
                        Mes = mesActual,
                        Title = formatoFecha.GetMonthName(mesActual).Substring(0, 3) + " " + anioDesde.ToString()
                    });
                }
            }
            else
            {
                for (int anioActual = anioDesde; anioActual <= anioHasta; anioActual++)
                {
                    if (anioActual == anioDesde)
                    {
                        for (int mesActual = mesDesde; mesActual <= 12; mesActual++)
                        {
                            _ranges.Add(new BarRange()
                            {
                                Anio = anioActual,
                                Mes = mesActual,
                                Title = formatoFecha.GetMonthName(mesActual).Substring(0, 3) + " " + anioActual.ToString()
                            });
                        }
                    }
                    if (anioActual != anioDesde && anioActual != anioHasta)
                    {
                        for (int mesActual = 1; mesActual <= 12; mesActual++)
                        {
                            _ranges.Add(new BarRange()
                            {
                                Anio = anioActual,
                                Mes = mesActual,
                                Title = formatoFecha.GetMonthName(mesActual).Substring(0, 3) + " " + anioActual.ToString()
                            });
                        }
                    }
                    if (anioActual == anioHasta)
                    {
                        for (int mesActual = 1; mesActual <= mesHasta; mesActual++)
                        {
                            _ranges.Add(new BarRange()
                            {
                                Anio = anioActual,
                                Mes = mesActual,
                                Title = formatoFecha.GetMonthName(mesActual).Substring(0, 3) + " " + anioActual.ToString()
                            });
                        }
                    } 
                }
            }

            return _ranges;
        }

        private decimal GetMedia(List<string> consultores, DBConnection connection = null)
        {
            decimal _costoFijo = 0;
            foreach (string _cons in consultores)
            {
                _costoFijo += ConsultorManagement.GetCustoFixo(_cons, connection).Value;
            }

            return Math.Round(_costoFijo / consultores.Count,2);
        }

        private Bar GetBarValues(int mesDesde, int anioDesde, int mesHasta, int anioHasta, string consultor)
        {
            decimal _custoFixo = 0;
            CultureInfo.CurrentCulture = CultureInfo.CreateSpecificCulture("es-ES");
            DateTimeFormatInfo formatoFecha = CultureInfo.CurrentCulture.DateTimeFormat;
            Random randomGen = new Random();
            List<string> _consultores = JsonConvert.DeserializeObject<List<string>>(consultor); 
            List<BarRange> _ranges = GetRanges(mesDesde, anioDesde, mesHasta, anioHasta);
            Bar _bar = new Bar()
            {
                Values = new List<BarValue>(),
                Labels = new List<string>(),
                Title = "Rango: " + formatoFecha.GetMonthName(mesDesde) + " " + anioDesde.ToString() + " - " + formatoFecha.GetMonthName(mesHasta) + " " + anioHasta.ToString()
            };

            _ranges.ForEach(x => _bar.Labels.Add(x.Title));

            new DBAccess().NonTransaction((connect) =>
            {
                List<Consultor> _lstConsultores = ConsultorManagement.GetConsultores(connect).Value;

                foreach (string _cons in _consultores)
                {
                    string _colorRGB = "rgb(" + randomGen.Next(0, 255) + "," + randomGen.Next(0, 255) + "," + randomGen.Next(0, 255) + ")";
                    string _consName = _lstConsultores.Where(x => x.Usuario == _cons).FirstOrDefault().Nombre;
                    BarValue _barValue = new BarValue()
                    {
                        backgroundColor = _colorRGB,
                        backgroundColorHover = _colorRGB,
                        borderColor = _colorRGB,
                        fill = false,
                        label = _consName,
                        type = "bar",
                        data = new List<decimal>()
                    };

                    foreach (BarRange _range in _ranges)
                    {
                        decimal _recitaLiquida = ConsultorManagement.GetReceitaLiquidaPorMes(_range.Mes, _range.Anio, _cons, connect).Value;
                        _barValue.data.Add(_recitaLiquida);
                    }

                    _bar.Values.Add(_barValue);
                }

                _custoFixo = GetMedia(_consultores, connect);
            });

            BarValue _barValueMedia = new BarValue()
            {
                backgroundColor = "red",
                backgroundColorHover = "red",
                borderColor = "red",
                fill = false,
                label = "Media",
                type = "line",
                data = new List<decimal>()
            };

            
            foreach(string _label in _bar.Labels)
            {
                _barValueMedia.data.Add(_custoFixo);
            }

            _bar.Values.Insert(0, _barValueMedia);

            return _bar;
        }

        private Pizza GetPizzaValues(int mesDesde, int anioDesde, int mesHasta, int anioHasta, string consultor)
        {
            List<string> _consultores = JsonConvert.DeserializeObject<List<string>>(consultor);
            List<decimal> _pizzaValues = new List<decimal>();
            List<string> _pizzaLabels = new List<string>();
            List<string> _pizzaColors = new List<string>();
            decimal _totalReceitas = 0;
            Random randomGen = new Random();

            new DBAccess().NonTransaction((connect) =>
            {
                List<Consultor> _lstConsultores = ConsultorManagement.GetConsultores(connect).Value;

                foreach (string _cons in _consultores)
                {
                    getColor:
                    string _colorRGB = "rgb(" + randomGen.Next(0, 255) + "," + randomGen.Next(0, 255) + "," + randomGen.Next(0, 255) + ")";
                    if (_pizzaColors.Count < 254 && _pizzaColors.Exists(x => x == _colorRGB))
                    {
                        goto getColor;
                    }
                    _pizzaColors.Add(_colorRGB);
                    Consultor _consultor = _lstConsultores.Where(x => x.Usuario == _cons).FirstOrDefault();
                    decimal _recitaLiquida = ConsultorManagement.GetRecitaLiquida(mesDesde, anioDesde, mesHasta, anioHasta, _cons, connect).Value;
                    _totalReceitas += _recitaLiquida;
                    _pizzaValues.Add(_recitaLiquida);
                    _pizzaLabels.Add(_consultor.Nombre);
                }
            });

            //actualizamos el porcentaje
            for (int i = 0; i < _pizzaValues.Count; i++)
            {
                _pizzaValues[i] = Math.Round((_pizzaValues[i] / _totalReceitas) * 100, 2);
            }

            CultureInfo.CurrentCulture = CultureInfo.CreateSpecificCulture("es-ES");
            DateTimeFormatInfo formatoFecha = CultureInfo.CurrentCulture.DateTimeFormat;
            Pizza _pizza = new Pizza()
            {
                Values = _pizzaValues,
                Labels = _pizzaLabels,
                Colors = _pizzaColors,
                Title = "Rango: " + formatoFecha.GetMonthName(mesDesde) + " " + anioDesde.ToString() + " - " + formatoFecha.GetMonthName(mesHasta) + " " + anioHasta.ToString()
            };

            return _pizza;
        }
        #endregion
    }
}