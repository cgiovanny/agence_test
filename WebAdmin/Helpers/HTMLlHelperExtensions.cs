﻿namespace WEB.Helpers
{
    using System.Web.Mvc;

    public static class HTMLlHelperExtensions
    {
        public static string IsSelected(this HtmlHelper html,string controller = null,string action = null, string cssClass = null )
        {
            if (string.IsNullOrEmpty(cssClass))
                cssClass = "active";
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];
            if (string.IsNullOrEmpty(controller))
                controller = currentController;
            if (string.IsNullOrEmpty(action))
                action = currentAction;
            return controller == currentController && action == currentAction ? cssClass : string.Empty;
        }
        public static string PageClass(this HtmlHelper html)
        {
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            return currentAction;
        }

        public static string GetActionActive(this HtmlHelper html)
        {
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            return currentAction;
        }

        public static string GetControllerActive(this HtmlHelper html)
        {
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];
            return currentController;
        }
    }   
}